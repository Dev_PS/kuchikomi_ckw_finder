# -*- coding: utf-8 -*-
import sys
import mysql.connector as mariadb


class CkwFinder(object):

    def find_kuchikomi_ckw(self, rc_admin_curr, aurora_curr, vc_id, master_id):
        query1 = "select A.id, A.keyword, A.activity_id, A.component_id from vc_keywords as A inner join vc_activity as B On A.activity_id = B.id where B.vc_id = {}".format(vc_id)
        rc_admin_curr.execute(query1)
        keywords = rc_admin_curr.fetchall()

        self.initialize_ckw_analyze_table(keywords, aurora_curr, master_id)

        table = "vc_account_id_{}.new_valuechain_splitted_table_{}".format(master_id, master_id)

        act_column = "activity_list_vc_{}".format(vc_id)
        aurora_curr.execute("alter table {} add column {} json".format(table, act_column))

        comp_column = "component_list_vc_{}".format(vc_id)
        aurora_curr.execute("alter table {} add column {} json".format(table, comp_column))

        column = "ckw_list_vc_{}".format(vc_id)
        aurora_curr.execute("alter table {} add column {} json".format(table, column))

        query2 = "select id, comment, kuchikomi_sentiment from {}".format(table)
        aurora_curr.execute(query2)
        kuchikomis_list = aurora_curr.fetchall()

        kuchikomi_list = [kuchikomi[1] for kuchikomi in kuchikomis_list]
        kuchikomi_id = [kuchikomi[0] for kuchikomi in kuchikomis_list]
        kuchikomi_sentiment = [kuchikomi[2] for kuchikomi in kuchikomis_list]

        keyword_list_org = [keyword[1].decode("utf-8") for keyword in keywords]
        keyword_list = [keyword[1].decode("utf-8") for keyword in keywords]
        keyword_id = [keyword[0] for keyword in keywords]
        activity_id = [keyword[2] for keyword in keywords]
        component_id = [keyword[3] for keyword in keywords]

        keyword_list_flag = [0] * len(keyword_list)
        keyword_list.sort(key=len)

        sorted_2d_keyword = []

        # Create 2D array of similar CKWs in
        for i in range(len(keyword_list)):
            if keyword_list_flag[i] == 1:
                continue

            temp = [keyword_list[i]]
            for j in range(i+1, len(keyword_list)):
                if keyword_list[i] in keyword_list[j]:
                    temp.append(keyword_list[j])
                    keyword_list_flag[j] = 1

            sorted_2d_keyword.append(temp)

        p_count = [0]*len(keyword_list_org)
        n_count = [0]*len(keyword_list_org)
        r_count = [0]*len(keyword_list_org)
        c_count = [0]*len(keyword_list_org)
        u_count = [0]*len(keyword_list_org)

        for z in range(len(kuchikomi_list)):

            json_data = ''
            activity_json_data = ''
            component_json_data = ''
            ckw_found = 0
            ckw_id_list = []
            # print("kuchikomi : {}".format(kuchikomi_list[z]))
            for count, ckw in enumerate(keyword_list):
                if ckw in kuchikomi_list[z]:
                    ckw_found = 1
                    # print("matched ckw : {}".format(ckw))
                    index = [(ix, iy) for ix, row in enumerate(sorted_2d_keyword) for iy, i in enumerate(row) if i == ckw]
                    # print("ckw list : {}".format(sorted_2d_keyword[index[0][0]]))
                    for k in range(index[0][1], len(sorted_2d_keyword[index[0][0]])):
                        if sorted_2d_keyword[index[0][0]][len(sorted_2d_keyword[index[0][0]]) - k - 1] in kuchikomi_list[z] and ckw in sorted_2d_keyword[index[0][0]][len(sorted_2d_keyword[index[0][0]]) - k - 1]:

                            ckw = sorted_2d_keyword[index[0][0]][len(sorted_2d_keyword[index[0][0]]) - k - 1]
                            # print("--------------- :superset ckw  : {}".format(ckw))
                            ckw_index = keyword_list_org.index(ckw)
                            ckw_org_index = keyword_id[ckw_index]
                            if ckw_org_index not in ckw_id_list:
                                json_data = json_data + '\'$."{}"\', {}, '.format(ckw_org_index, ckw_org_index)
                                activity_json_data = activity_json_data + '\'$."{}"\', {}, '.format(activity_id[ckw_index], activity_id[ckw_index])
                                component_json_data = component_json_data + '\'$."{}"\', {}, '.format(component_id[ckw_index], component_id[ckw_index])
                                ckw_id_list.append(ckw_org_index)

                                # ckw count

                                if kuchikomi_sentiment[z] == 'positive':
                                    p_count[ckw_index] += 1
                                if kuchikomi_sentiment[z] == 'negative':
                                    n_count[ckw_index] += 1
                                if kuchikomi_sentiment[z] == 'request':
                                    r_count[ckw_index] += 1
                                if kuchikomi_sentiment[z] == 'critical':
                                    c_count[ckw_index] += 1
                                if kuchikomi_sentiment[z] == 'unrelated':
                                    u_count[ckw_index] += 1

            if ckw_found == 1:
                json_data = json_data[:-2]
                query3 = "update {} set {} = JSON_SET(json_if, {}) where id = {}".format(table, column, json_data, kuchikomi_id[z])
                query3 = query3.replace("json_if", "IF(" + column + " IS NULL, '{}', " + column + ")", 1)
                # print(query3)
                aurora_curr.execute(query3)

                activity_json_data = activity_json_data[:-2]
                query3 = "update {} set {} = JSON_SET(json_if, {}) where id = {}".format(table, act_column, activity_json_data,
                                                                                         kuchikomi_id[z])
                query3 = query3.replace("json_if", "IF(" + act_column + " IS NULL, '{}', " + act_column + ")", 1)
                # print(query3)
                aurora_curr.execute(query3)

                component_json_data = component_json_data[:-2]
                query3 = "update {} set {} = JSON_SET(json_if, {}) where id = {}".format(table, comp_column, component_json_data,
                                                                                         kuchikomi_id[z])
                query3 = query3.replace("json_if", "IF(" + comp_column + " IS NULL, '{}', " + comp_column + ")", 1)
                # print(query3)
                aurora_curr.execute(query3)



        for col in range(0, len(keyword_list_org)):
            table = "vc_account_id_{}.new_valuechain_analyzed_table_{}".format(master_id, master_id)
            query3 = "update {} set positive_count = {}, negative_count = {}, request_count = {}," \
                     "critical_count = {},unrelated_count = {} where ckw_id = {}".format(table, p_count[col],
                                                                                         n_count[col],
                                                                                         r_count[col],
                                                                                         c_count[col],
                                                                                         u_count[col],
                                                                                         keyword_id[col])
            print(query3)
            aurora_curr.execute(query3)



    def initialize_ckw_analyze_table(self, keywords, aurora_curr, master_id):

        table = "vc_account_id_{}.new_valuechain_analyzed_table_{}".format(master_id, master_id)
        dst_column = " ckw_id, keyword, activity_id, component_id "

        for row in keywords:
            ckw_id = row[0]
            keyword = row[1].decode("utf-8")
            activity_id = row[2]
            component_id = row[3]

            query = "insert into {} ( ckw_id, keyword, activity_id, component_id ) values ({}, '{}' ,{} ,{})".format(table, ckw_id, keyword, activity_id, component_id)
            aurora_curr.execute(query)

        pass

    def ckw_handler(self):

        if len(sys.argv) != 3:
            print("Usages : python ckw_finder vc_id master_id")
            exit(0)

        vc_id = sys.argv[1]
        master_id = sys.argv[2]

        print(vc_id)
        print(master_id)
        print("connecting to rc_admin.....")
        # get connection with rc_admin db
        conn_secondary = conn = mariadb.connect(user="rc_user",
                               password="2e9hstUuEN",
                               host="13.113.108.206",
                               database="rc_admin")

        print("rc_admin connected .....")
        conn_secondary.autocommit = True
        rc_admin_curr = conn_secondary.cursor()

        # get connection with aurora splitted db
        conn_primary = conn = mariadb.connect(user="rc_user",
                               password="rW92UzVY",
                               host="new-rc-db-instance-cluster.cluster-cpohrfy1rutv.ap-northeast-1.rds.amazonaws.com",
                               database="vc_account_id_38")
        print("aurora connected .....")
        conn_primary.autocommit = True
        aurora_curr = conn_primary.cursor()

        self.find_kuchikomi_ckw(rc_admin_curr, aurora_curr, vc_id, master_id)

        conn_secondary.commit()
        conn_primary.commit()

        if rc_admin_curr is not None:
            rc_admin_curr.close()

        if conn_secondary is not None:
            conn_secondary.close()

        if aurora_curr is not None:
            aurora_curr.close()

        if conn_primary is not None:
            conn_primary.close()


test = CkwFinder()
test.ckw_handler()
