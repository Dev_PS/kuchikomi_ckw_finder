# -*- coding: utf-8 -*-
import sys
import mysql.connector as mariadb
from datetime import datetime

class CkwFinder(object):

    def find_kuchikomi_ckw(self, rc_admin_curr, aurora_curr, vc_id, master_id):
        query1 = "select A.id, A.keyword, A.activity_id, A.component_id from vc_keywords as A inner join vc_activity as B On A.activity_id = B.id where B.vc_id = {}".format(vc_id)
        rc_admin_curr.execute(query1)
        keywords = rc_admin_curr.fetchall()

        # self.initialize_ckw_analyze_table(keywords, aurora_curr, master_id)

        table = "vc_account_id_{}.new_valuechain_splitted_table_{}".format(master_id, master_id)

        query2 = "select * from {} where dupilcate_kuchikomi = 0".format(table)
        aurora_curr.execute(query2)
        kuchikomis_list = aurora_curr.fetchall()

        keyword_list_org = [keyword[1].decode("utf-8") for keyword in keywords]
        keyword_list = [keyword[1].decode("utf-8") for keyword in keywords]
        keyword_id = [keyword[0] for keyword in keywords]
        activity_id = [keyword[2] for keyword in keywords]
        component_id = [keyword[3] for keyword in keywords]

        keyword_list_flag = [0] * len(keyword_list)
        keyword_list.sort(key=len)

        sorted_2d_keyword = []

        # Create 2D array of similar CKWs in
        for i in range(len(keyword_list)):
            if keyword_list_flag[i] == 1:
                continue

            '''
            arrange the keywords in assending order of there length
            take first ckw, find next matching ckw and put them in a single list
            finally build a 2D list of all 1D list
            '''

            temp = [keyword_list[i]]
            for j in range(i+1, len(keyword_list)):
                if keyword_list[i] in keyword_list[j]:
                    temp.append(keyword_list[j])
                    keyword_list_flag[j] = 1

            sorted_2d_keyword.append(temp)

        for z in range(len(kuchikomis_list)):

            # print("kuchikomis_list[0][3] : {}".format(kuchikomis_list[z]['comment']))

            json_data = ''
            ckw_found = 0
            ckw_id_list = []
            ckw_list = ''
            db_updated = -1
            item = {}
            #print("kuchikomi : {}".format(kuchikomis_list[z]['comment']))
            for count, ckw in enumerate(keyword_list):
                if ckw in kuchikomis_list[z]['comment']:

                    ckw_found = 1
                    #print("matched ckw : {}".format(ckw))

                    # find the index of matched ckw in sorted_2d_keyword
                    index = [(ix, iy) for ix, row in enumerate(sorted_2d_keyword) for iy, i in enumerate(row) if i == ckw]
                    #print("ckw list : {}".format(sorted_2d_keyword[index[0][0]]))

                    '''
                    from the next index to the last of that row check for all ckws whether they are present in 
                    same kuchikomi
                    '''
                    for k in range(index[0][1], len(sorted_2d_keyword[index[0][0]])):
                        if sorted_2d_keyword[index[0][0]][len(sorted_2d_keyword[index[0][0]]) - k - 1] in kuchikomis_list[z]['comment'] and ckw in sorted_2d_keyword[index[0][0]][len(sorted_2d_keyword[index[0][0]]) - k - 1]:

                            ckw = sorted_2d_keyword[index[0][0]][len(sorted_2d_keyword[index[0][0]]) - k - 1]

                            ckw_index = keyword_list_org.index(ckw)
                            ckw_org_index = keyword_id[ckw_index]
                            if ckw_org_index not in ckw_id_list:
                                #print("--------------- :superset ckw  : {}".format(ckw))
                                #print("--------------- :superset ckw_org_index  : {}".format(ckw_org_index))
                                #print("--------------- :superset component_id  : {}".format(component_id[ckw_index]))
                                #print("--------------- :superset activity_id  : {}".format(activity_id[ckw_index]))

                                # print("kuchikomis_list[z][23] : {}".format(kuchikomis_list[z]))
                                if kuchikomis_list[z]['dupilcate_kuchikomi'] == 0 and db_updated == -1:
                                    #print("Inside if : {}".format(ckw))
                                    query = "update  {} set ckw_matched = '{}', id_ckw_matched = {}, component_id_of_ckw_matched = {}, activity_id_of_ckw_matched = {} where id = {}".format(table, ckw, ckw_org_index, component_id[ckw_index], activity_id[ckw_index], kuchikomis_list[z]['id'])
                                    #print(query)
                                    db_updated = kuchikomis_list[z]['id']
                                    aurora_curr.execute(query)
                                else:
                                    item["kuchikomi_id"] = kuchikomis_list[z]["kuchikomi_id"]
                                    item["splitted_id"] = kuchikomis_list[z]["splitted_id"]
                                    item["comment"] = kuchikomis_list[z]["comment"]
                                    item["full_kuchikomi"] = kuchikomis_list[z]["full_kuchikomi"]
                                    item["kuchikomi_sentiment_auto"] = kuchikomis_list[z]["kuchikomi_sentiment_auto"]
                                    item["kuchikomi_sentiment_updated"] = kuchikomis_list[z]["kuchikomi_sentiment_updated"]
                                    item["kuchikomi_sentiment_calculated"] = kuchikomis_list[z]["kuchikomi_sentiment_calculated"]
                                    item["status"] = kuchikomis_list[z]["status"]
                                    item["kuchikomi_language"] = kuchikomis_list[z]["kuchikomi_language"]
                                    item["duplicate_of"] = kuchikomis_list[z]["duplicate_of"]
                                    item["ckw_id"] = kuchikomis_list[z]["ckw_id"]
                                    item["ota_name"] = kuchikomis_list[z]["ota_name"]
                                    item["hotel_id"] = kuchikomis_list[z]["hotel_id"]
                                    item["post_date"] = kuchikomis_list[z]["post_date"]
                                    date = datetime.now()
                                    item["analysis_date"] = date.strftime('%Y-%m-%d %I:%M:%S')
                                    item["ckw_matched"] = ckw
                                    item["id_ckw_matched"] = ckw_org_index
                                    item["component_id_of_ckw_matched"] = component_id[ckw_index]
                                    item["activity_id_of_ckw_matched"] = activity_id[ckw_index]
                                    item["target_activity_id"] = kuchikomis_list[z]["target_activity_id"]
                                    item["target_component_id"] = kuchikomis_list[z]["target_component_id"]
                                    item["dupilcate_kuchikomi"] = kuchikomis_list[z]["dupilcate_kuchikomi"]
                                    item["updater_name"] = kuchikomis_list[z]["updater_name"]
                                    item["all_matched_ckw"] = kuchikomis_list[z]["all_matched_ckw"]
                                    item["data_source_id"] = kuchikomis_list[z]["data_source_id"]
                                    item["multi_ckw_duplicate"] = 1 # this kuchikomi is having multiple ckws so duplicated

                                    db_key = []
                                    db_value = []
                                    db_item = []
                                    db_key_set = []
                                    db_key_value = []
                                    # Build query string
                                    for (key, value) in item.items():
                                        db_key.append(key)
                                        db_value.append(value)
                                        db_item.append('%s')
                                        db_key_set.append(key + '=%s')
                                        db_key_value.append(value)

                                    db_value.extend(db_key_value)

                                    query = db_value
                                    # print(query)

                                    # Use Lightweight transaction
                                    aurora_curr.execute(
                                        "INSERT INTO " + table + " (" + ', '.join(db_key) + ")" +
                                        " VALUES (" + ', '.join(db_item) + ")  ON DUPLICATE KEY UPDATE " + ', '.join(
                                            db_key_set) + "", db_value)

                                ckw_id_list.append(ckw_org_index)
                                ckw_list = ckw_list + ", " + ckw

            if ckw_found == 1:
                query = "update  {} set all_matched_ckw = '{}' where kuchikomi_id = {} and splitted_id = {}".format(table, ckw_list[2:], kuchikomis_list[z]['kuchikomi_id'], kuchikomis_list[z]['splitted_id'])
                #print(query)
                aurora_curr.execute(query)


    def initialize_ckw_analyze_table(self, keywords, aurora_curr, master_id):

        table = "vc_account_id_{}.new_valuechain_analyzed_table_{}".format(master_id, master_id)
        dst_column = " ckw_id, keyword, activity_id, component_id "

        for row in keywords:
            ckw_id = row[0]
            keyword = row[1].decode("utf-8")
            activity_id = row[2]
            component_id = row[3]

            query = "insert into {} ( ckw_id, keyword, activity_id, component_id ) values ({}, '{}' ,{} ,{})".format(table, ckw_id, keyword, activity_id, component_id)
            # aurora_curr.execute(query)

        pass

    def ckw_handler(self):

        if len(sys.argv) != 3:
            print("Usages : python ckw_finder_updated vc_id master_id")
            exit(0)

        vc_id = sys.argv[1]
        master_id = sys.argv[2]

        database = 'vc_account_id_' + str(master_id)

        print("connecting to rc_admin.....")
        # get connection with rc_admin db
        conn_secondary = mariadb.connect(user="rc_user",
                               password="rW92UzVY",
                               host="rc-prd-dbinstance-rcdb-0001-cluster.cluster-cpohrfy1rutv.ap-northeast-1.rds.amazonaws.com",
                               # host="rc-dev-rds-aurora-0001-cluster.cluster-cpohrfy1rutv.ap-northeast-1.rds.amazonaws.com",
                               database="rc_admin")

        print("rc_admin connected .....")
        conn_secondary.autocommit = True
        rc_admin_curr = conn_secondary.cursor()
        print("rc_admin connected .....")

        # get connection with aurora splitted db
        conn_primary = mariadb.connect(user="rc_user",
                               password="rW92UzVY",
                               host="rc-prd-dbinstance-rcdb-0001-cluster.cluster-cpohrfy1rutv.ap-northeast-1.rds.amazonaws.com",
                               # host="rc-dev-rds-aurora-0001-cluster.cluster-cpohrfy1rutv.ap-northeast-1.rds.amazonaws.com",
                               database=database)
        print("aurora connected .....")
        conn_primary.autocommit = True
        aurora_curr = conn_primary.cursor(dictionary=True)

        self.find_kuchikomi_ckw(rc_admin_curr, aurora_curr, vc_id, master_id)

        conn_secondary.commit()
        conn_primary.commit()

        if rc_admin_curr is not None:
            rc_admin_curr.close()

        if conn_secondary is not None:
            conn_secondary.close()

        if aurora_curr is not None:
            aurora_curr.close()

        if conn_primary is not None:
            conn_primary.close()


test = CkwFinder()

test.ckw_handler()
