# -*- coding: utf-8 -*-
import sys
import mysql.connector as mariadb
import requests
import json
from datetime import datetime

class PsHotelIdFinder(object):

    def find_hotelmaster_id(self, aurora_curr, table_name):
        ota = table_name.split("_")
        not_found = []
        not_found_count = 0
        query1 = "select id, hotel_id from {}".format(table_name)
        aurora_curr.execute(query1)
        kuchikomi_info = aurora_curr.fetchall()

        print(len(kuchikomi_info))

        for i in range(len(kuchikomi_info)):
            myResponse = requests.get('http://api.repchecker.jp/hotel-master/ota_seq_to_master_seq?ota_seq='+kuchikomi_info[i][1]+'&ota='+ota[0]+'&access_key=4e1349a647151150c50ee4b70254335b')

            if (myResponse.ok):
                print("id : "+ str(kuchikomi_info[i][0]) + "  |  Not found : " + str(not_found_count))
                # Loading the response data into a dict variable
                # json.loads takes in only binary or string variables so using content to fetch binary content
                # Loads (Load String) takes a Json file and converts into python data structure (dict or list, depending on JSON)
                jData = json.loads(myResponse.content.decode('utf-8'))
                if len(jData['response']) > 0:
                    ps_hotel_id = jData['response'][0]['repchecker_keyword_mst_seq']
                    self.set_ps_hotel_id(ps_hotel_id, kuchikomi_info[i][0], table_name, aurora_curr)
                else:
                    not_found.append(kuchikomi_info[i][1])
                    not_found_count = not_found_count + 1

            else:
                # If response code is not ok (200), print the resulting http error code with description
                myResponse.raise_for_status()

        print("all not found hotel ids : {}".format(not_found))

    def set_ps_hotel_id(self, ps_hotel_id, kuchikomi_id, table_name, aurora_curr):
        query1 = "update {} set ps_hotel_id = {} where id = {}".format(table_name, ps_hotel_id, kuchikomi_id)
        aurora_curr.execute(query1)

    def db_connecter(self):

        if len(sys.argv) != 2:
            print("Usages : python ota_to_hotelmasterPsinc_hotelid_change.py table_name")
            exit(0)

        table_name = sys.argv[1]

        print("Selected Table : {}".format(table_name))


        # get connection with aurora splitted db
        conn_primary = mariadb.connect(user="rc_user",
                               password="rW92UzVY",
                               host="new-rc-db-instance-cluster.cluster-cpohrfy1rutv.ap-northeast-1.rds.amazonaws.com",
                               database="new_valuechain_db")
        print("aurora connected .....")
        conn_primary.autocommit = True
        aurora_curr = conn_primary.cursor()

        self.find_hotelmaster_id( aurora_curr, table_name)

        conn_primary.commit()

        if aurora_curr is not None:
            aurora_curr.close()

        if conn_primary is not None:
            conn_primary.close()


test = PsHotelIdFinder()

test.db_connecter()
